# tea-cloc

Lines of code counter for Gitea

## Usage

1. Clone the repository `git clone https://codeberg.org/qwerty287/tea-cloc.git` or download [a binary](https://codeberg.org/qwerty287/tea-cloc/releases/latest)
2. Create a file called `config.ini` that holds your [configuration](#config)
3. Start the server
   1. if you downloaded the binary, just run `./tea-cloc`
   2. if you cloned the source code: just run `go run main.go`
   3. if you cloned the source code: or compile a binary with `go build` and run with `./tea-cloc`
4. for every repository you want to use tea-cloc, open the webhook settings of the repository and create a new `Gitea` webhook (Trigger On and Branch filter depend on what you would like to use, of course)
    * Target URL: the URL used for your tea-cloc server, with `/hook` path (e.g. `http://localhost:8080/hook`)
    * HTTP Method: `POST`
    * POST Content Type: `application/json`
    * Secret: the secret your [config](#config) contains
    * Trigger On: Custom Events...
      * Repository Events
        * Push
      * Pull Request Events
        * Pull Request
        * Pull Request Synchronized
    * Branch filter: `*`
    * Active: ✅

## Config

Configuration is stored as INI file in `config.ini`. If you need a "global" file location, the secondary one is `/etc/tea-cloc.ini`.

Sections:

`tokens`
* contains instance's tokens (and usernames) in the format `"<instance>"=<username>:<token>`
  * must use full instance name how it is returned in webhooks. For example, `http://localhost:3000` and not `localhost:3000` (**without** a trailing slash)
  * if you would like to work with private repositories, the `username` must be set. It will allow Git to clone them. The corresponding user must have read permissions to repository and must match with the user owning the token.

`database`
* `TYPE`: `mysql`, `postgresql`, `mssql` or `sqlite3` (default `sqlite3`)
* `PATH`: path to SQLite database (default `db.sqlite`)
* `TIMEOUT`: timeout for SQLite database (default 500)
* `HOST`: database host for MySQL, MSSQL or PostgreSQL database
* `NAME`: database name for MySQL, MSSQL or PostgreSQL database
* `USER`: database user for MySQL, MSSQL or PostgreSQL database
* `PASSWORD`: database password for MySQL, MSSQL or PostgreSQL database
* `SSL`: whether to use SSL/TLS for MySQL, MSSQL or PostgreSQL database
* `CHARSET`: database charset for MySQL database

`server`
* `DEBUG_MODE`: run Gin in debug mode (default false)
* `PORT`: port to listen to (default 8080, can be overridden using the `-port` flag)
* `DOMAIN`: domain of the server
* `SECRET`: secret to protect webhooks
* `ALLOW_PUSH`: allow push events (default true)
* `ALLOW_PR`: allow pull request events (default true)
* `TLS_MODE`: `none`, `customcert` or `letsencrypt` (default `none`). If you use `letsencrypt`, you accept the [terms and conditions of Let's Encrypt](https://letsencrypt.org/documents/LE-SA-v1.2-November-15-2017.pdf).
* `TLS_CERT`: path to TLS certificate. Only used if `server.TLS_MODE` is `customcert`.
* `TLS_PRIV`: path to TLS private key. Only used if `server.TLS_MODE` is `customcert`.
* `COMMENT_TYPE`: type of Gitea comments, `table`, `json`, `yaml` or `xml` (default `table`)
* `STATUS_CONTEXT`: context of the commit status sent to Gitea (default `tea-cloc`)
* `STATUS_CONTEXT_PR`: context of the commit status sent to Gitea for PR checks (default `tea-cloc (PR)`)
* `SKIP`: commits containing this will be skipped (default `[skip cloc]`)
* `OWNER`: owner of repos where counting is allowed
* `REPO`: name of repos where counting is allowed (this only affects the repo name and not the owner)

`data`
* `TMP`: use a temporary directory (e.g. usually under Unix as a subdirectory of `/tmp`). **DO NOT DISABLE THIS IN A PRODUCTIVE ENVIRONMENT.** This is necessary to run multiple checks at one time in one repository (default true)
* `LOCATION`: where to save temporary data to count LOC, ignored if `data.TMP` is `true` (default `data/`)
* `KEEP`: keep data directories after counting (default false). Not recommended.

Simple example configuration suitable for debugging:
```ini
[tokens]
"http://localhost:3000"=username:33ab5639bfd8e7b95eb1d8d0b87781d4ffea4d5d

[server]
DEBUG_MODE=true

[database]
TYPE=sqlite3
PATH=db.sqlite
```