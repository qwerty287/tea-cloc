package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	"codeberg.org/qwerty287/tea-cloc/models"
	"codeberg.org/qwerty287/tea-cloc/modules"
	"codeberg.org/qwerty287/tea-cloc/modules/config"
	"codeberg.org/qwerty287/tea-cloc/routes"

	"github.com/gin-gonic/autotls"
	"github.com/gin-gonic/gin"
)

const VERSION = "v1.1.0"

func main() {
	if err := config.InitConfig(); err != nil {
		log.Fatal(err)
	}

	portFlag := flag.Int("port", config.Config.Server.Port, "port on which the server listens")
	versionFlag := flag.Bool("version", false, "print version and exit")
	flag.Parse()

	if *versionFlag {
		fmt.Println(VERSION)
		os.Exit(0)
	}

	config.Config.Server.Port = *portFlag

	if err := models.InitEngine(); err != nil {
		log.Fatal(err)
	}

	if !config.Config.Server.DebugMode {
		gin.SetMode(gin.ReleaseMode)
	}
	r := gin.Default()
	r.POST("/hook", func(c *gin.Context) {
		modules.StartCheck(c)
	})
	if config.Config.Server.AllowPush {
		r.GET("/:owner/:repo/:sha", routes.ViewCount)
	}

	tlsMode := config.Config.Server.TLSMode
	if tlsMode == "letsencrypt" {
		if err := autotls.Run(r, config.Config.Server.Domain); err != nil {
			log.Fatal(err)
		}
	} else if tlsMode == "customcert" {
		if err := http.ListenAndServeTLS(
			getConnStr(),
			config.Config.Server.TLSCert,
			config.Config.Server.TLSPriv,
			r,
		); err != nil {
			log.Fatal(err)
		}
	} else {
		if tlsMode != "none" && tlsMode != "" {
			log.Printf("the TLS mode %s is not supported\n", tlsMode)
		}
		if err := r.Run(getConnStr()); err != nil {
			log.Fatal(err)
		}
	}
}

func getConnStr() string {
	host := config.Config.Server.Domain
	port := config.Config.Server.Port
	return fmt.Sprintf("%s:%d", host, port)
}
