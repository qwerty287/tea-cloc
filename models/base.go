package models

import (
	"fmt"
	"net/url"
	"os"
	"path"
	"strings"

	"codeberg.org/qwerty287/tea-cloc/modules/config"

	// required as drivers for Xorm.
	_ "github.com/denisenkom/go-mssqldb"
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
	_ "github.com/mattn/go-sqlite3"

	"xorm.io/xorm"
)

var engine *xorm.Engine

func InitEngine() error {
	if engine == nil {
		param := "?"
		if strings.Contains(config.Config.Database.Name, param) {
			param = "&"
		}

		var err error
		var fullPath string

		switch config.Config.Database.Type {
		case "mysql":
			connType := "tcp"
			if len(config.Config.Database.Host) > 0 && config.Config.Database.Host[0] == '/' { // looks like a unix socket
				connType = "unix"
			}
			tls := config.Config.Database.SSL
			if tls == "disable" { // allow (Postgres-inspired) default value to work in MySQL
				tls = "false"
			}
			fullPath = fmt.Sprintf("%s:%s@%s(%s)/%s%scharset=%s&parseTime=true&tls=%s",
				config.Config.Database.User, config.Config.Database.Password, connType, config.Config.Database.Host, config.Config.Database.Name, param, config.Config.Database.Charset, tls)
		case "postgresql":
			info := config.Config.Database.Host
			host, port := "127.0.0.1", "5432"
			if strings.Contains(info, ":") && !strings.HasSuffix(info, "]") {
				idx := strings.LastIndex(info, ":")
				host = info[:idx]
				port = info[idx+1:]
			} else if len(info) > 0 {
				host = info
			}
			if host[0] == '/' { // looks like a unix socket
				fullPath = fmt.Sprintf("postgres://%s:%s@:%s/%s%ssslmode=%s&host=%s",
					url.PathEscape(config.Config.Database.User), url.PathEscape(config.Config.Database.Password), port, config.Config.Database.Name, param, config.Config.Database.SSL, host)
			} else {
				fullPath = fmt.Sprintf("postgres://%s:%s@%s:%s/%s%ssslmode=%s",
					url.PathEscape(config.Config.Database.User), url.PathEscape(config.Config.Database.Password), host, port, config.Config.Database.Name, param, config.Config.Database.SSL)
			}
		case "mssql":
			info := config.Config.Database.Host
			host, port := "127.0.0.1", "0"
			if strings.Contains(info, ":") {
				host = strings.Split(info, ":")[0]
				port = strings.Split(info, ":")[1]
			} else if strings.Contains(info, ",") {
				host = strings.Split(info, ",")[0]
				port = strings.TrimSpace(strings.Split(info, ",")[1])
			} else if len(info) > 0 {
				host = info
			}
			fullPath = fmt.Sprintf("server=%s; port=%s; database=%s; user id=%s; password=%s;", host, port, config.Config.Database.Name, config.Config.Database.User, config.Config.Database.Password)
		case "sqlite3":
			if err := os.MkdirAll(path.Dir(config.Config.Database.Path), os.ModePerm); err != nil {
				return err
			}
			fullPath = fmt.Sprintf("file:%s?cache=shared&mode=rwc&_busy_timeout=%d&_txlock=immediate", config.Config.Database.Path, config.Config.Database.Timeout)
		default:
			return fmt.Errorf("unsupported database type specified")
		}

		engine, err = xorm.NewEngine(config.Config.Database.Type, fullPath)
		if err != nil {
			return err
		}

		return sync()
	}

	return nil
}

func sync() error {
	if err := engine.Sync2(new(LocalRepo)); err != nil {
		return err
	}

	if err := engine.Sync2(new(LanguageStat)); err != nil {
		return err
	}

	return engine.Sync2(new(LocalPR))
}
