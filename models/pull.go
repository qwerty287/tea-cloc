package models

import "code.gitea.io/sdk/gitea"

type PRHook struct {
	Number      int64             `json:"number"`
	PullRequest *PullRequest      `json:"pull_request"`
	Repository  *gitea.Repository `json:"repository"`
}

type PullRequest struct {
	Number int64   `json:"number"`
	Title  string  `json:"title"`
	Base   *Branch `json:"base"`
	Head   *Branch `json:"head"`
}

func (p *PullRequest) Local() (*LocalPR, error) {
	r, has, err := RepoByOwnerNameInstance(RepositoryInstance(p.Base.Repo), p.Base.Repo.Owner.UserName, p.Base.Repo.Name)
	if err != nil {
		return nil, err
	}
	if !has {
		repo := LocalRepo{
			Instance: RepositoryInstance(p.Base.Repo),
			Owner:    p.Base.Repo.Owner.UserName,
			Name:     p.Base.Repo.Name,
		}
		_, err = engine.Insert(&repo)
		if err != nil {
			return nil, err
		}
		r = &repo
	}
	return &LocalPR{
		RepoID: r.ID,
		Index:  p.Number,
	}, nil
}

type Branch struct {
	Ref  string            `json:"ref"`
	Repo *gitea.Repository `json:"repo"`
	SHA  string            `json:"sha"`
}

type LocalPR struct {
	ID        int64 `xorm:"pk autoincr"`
	Index     int64
	RepoID    int64
	CommentID int64
}

func PRByOwnerNameIndex(instance, owner, name string, index int64) (*LocalPR, bool, error) {
	r, has, err := RepoByOwnerNameInstance(instance, owner, name)
	if err != nil || !has {
		return nil, false, err
	}
	var pr LocalPR
	has, err = engine.Where("`repo_i_d` = ?", r.ID).And("`index` = ?", index).Get(&pr)
	return &pr, has, err
}

func NewPR(pr *LocalPR) (int64, error) {
	return engine.Insert(pr)
}
