package models

import (
	"strings"

	"code.gitea.io/sdk/gitea"
)

type RepoHook struct {
	Ref        string            `json:"ref"`
	HeadCommit *Commit           `json:"head_commit"`
	Repository *gitea.Repository `json:"repository"`
}

func LocalRepository(r *gitea.Repository) *LocalRepo {
	return &LocalRepo{
		Instance: RepositoryInstance(r),
		Name:     r.Name,
		Owner:    r.Owner.UserName,
	}
}

func RepositoryInstance(r *gitea.Repository) string {
	url := strings.Split(r.CloneURL, "/")
	url = url[:len(url)-2]
	return strings.Join(url, "/")
}

func RepositoryDataDir(r *gitea.Repository, base string) string {
	return base + r.Name
}

type User struct {
	Login string `json:"login"`
}

type Commit struct {
	SHA     string `json:"id"`
	Message string `json:"message"`
}

type LocalRepo struct {
	ID       int64 `xorm:"pk autoincr"`
	Instance string
	Name     string
	Owner    string
}

func RepoByOwnerNameInstance(instance, owner, name string) (*LocalRepo, bool, error) {
	var repo LocalRepo
	has, err := engine.Where("`instance` = ?", instance).And("`name` = ?", name).And("`owner` = ?", owner).Get(&repo)
	return &repo, has, err
}

func ReposByOwnerName(owner, name string) ([]*LocalRepo, error) {
	var repos []*LocalRepo
	err := engine.Where("`name` = ?", name).And("`owner` = ?", owner).Find(&repos)
	return repos, err
}

func NewRepo(r *LocalRepo) (int64, error) {
	return engine.Insert(r)
}
