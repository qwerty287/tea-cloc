package models

import (
	"github.com/hhatto/gocloc"
)

type LanguageStat struct {
	ID        int64  `xorm:"pk autoincr" json:"-" xml:"-" yaml:"-"`
	RepoID    int64  `json:"-" xml:"-" yaml:"-"`
	CommitSHA string `json:"commit_sha"  xml:"commit_sha" yaml:"commitSHA"`

	Name     string `json:"name" xml:"name" yaml:"name"`
	FilesNum int    `json:"files_num"  xml:"files_num" yaml:"filesNum"`
	Code     int32  `json:"code" xml:"code" yaml:"code"`
	Comments int32  `json:"comments" xml:"comments" yaml:"comments"`
	Blanks   int32  `json:"blanks" xml:"blanks" yaml:"blanks"`
	Total    int32  `json:"total" xml:"total" yaml:"total"`
}

func FromGocloc(lang *gocloc.Language, repo int64, sha string) *LanguageStat {
	return &LanguageStat{
		RepoID:    repo,
		CommitSHA: sha,

		Name:     lang.Name,
		FilesNum: len(lang.Files),
		Code:     lang.Code,
		Comments: lang.Comments,
		Blanks:   lang.Blanks,
		Total:    lang.Total,
	}
}

func HasIndexed(repo int64, sha string) (bool, error) {
	count, err := engine.Table("language_stat").Where("`repo_i_d` = ?", repo).And("`commit_s_h_a` = ?", sha).Count()
	return count > 0, err
}

func StatsByRepoSHA(repo int64, sha string) ([]*LanguageStat, error) {
	var langs []*LanguageStat
	err := engine.Where("`repo_i_d` = ?", repo).And("`commit_s_h_a` = ?", sha).Find(&langs)
	return langs, err
}

func NewStat(stat *LanguageStat) (int64, error) {
	return engine.Insert(stat)
}
