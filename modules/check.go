package modules

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"codeberg.org/qwerty287/tea-cloc/models"
	"codeberg.org/qwerty287/tea-cloc/modules/config"
	"gopkg.in/yaml.v3"

	"code.gitea.io/sdk/gitea"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-git/go-git/v5"
	git_config "github.com/go-git/go-git/v5/config"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/hhatto/gocloc"
)

var processor = gocloc.NewProcessor(gocloc.NewDefinedLanguages(), gocloc.NewClocOptions())

type localMsg struct {
	Msg string `json:"message"`
}

func StartCheck(c *gin.Context) {
	if c.Request.Header["X-Gitea-Event"] == nil || len(c.Request.Header["X-Gitea-Event"]) != 1 {
		c.JSON(http.StatusNotFound, localMsg{"missing header"})
		return
	}

	secret := config.Config.Server.Secret
	if secret != "" {
		sigRaw := c.Request.Header["X-Gitea-Signature"]
		if len(sigRaw) != 1 {
			c.JSON(http.StatusBadRequest, localMsg{"bad secret header"})
			return
		}
		sig := sigRaw[0]

		var body []byte
		if cb, ok := c.Get(gin.BodyBytesKey); ok {
			if cbb, ok := cb.([]byte); ok {
				body = cbb
			}
		}
		if body == nil {
			var err error
			body, err = io.ReadAll(c.Request.Body)
			if err != nil {
				c.JSON(http.StatusInternalServerError, localMsg{err.Error()})
				return
			}
			c.Set(gin.BodyBytesKey, body)
		}

		sig256 := hmac.New(sha256.New, []byte(secret))
		_, err := io.Writer(sig256).Write(body)
		if err != nil {
			c.JSON(http.StatusInternalServerError, localMsg{err.Error()})
			return
		}

		sigExpected := hex.EncodeToString(sig256.Sum(nil))

		if sig != sigExpected {
			c.JSON(http.StatusUnauthorized, localMsg{"bad secret"})
			return
		}

	}

	switch c.Request.Header["X-Gitea-Event"][0] {
	case "push":
		if !config.Config.Server.AllowPush {
			c.JSON(404, localMsg{"event not supported"})
			return
		}
		// push into repository or branch created
		var h models.RepoHook

		if err := c.ShouldBindBodyWith(&h, binding.JSON); err != nil {
			c.JSON(http.StatusBadRequest, localMsg{err.Error()})
			return
		}

		if config.Config.Server.Owner != "" && h.Repository.Owner.UserName != config.Config.Server.Owner {
			c.JSON(403, localMsg{"owner not allowed"})
			return
		}
		if config.Config.Server.Repo != "" && h.Repository.Name != config.Config.Server.Repo {
			c.JSON(403, localMsg{"repo not allowed"})
			return
		}

		go startCheckPush(&h)
	case "pull_request":
		if !config.Config.Server.AllowPR {
			c.JSON(404, localMsg{"event not supported"})
			return
		}
		var h models.PRHook

		if err := c.ShouldBindBodyWith(&h, binding.JSON); err != nil {
			c.JSON(http.StatusBadRequest, localMsg{err.Error()})
			return
		}

		if config.Config.Server.Owner != "" && h.Repository.Owner.UserName != config.Config.Server.Owner {
			c.JSON(403, localMsg{"owner not allowed"})
			return
		}
		if config.Config.Server.Repo != "" && h.Repository.Name != config.Config.Server.Repo {
			c.JSON(403, localMsg{"repo not allowed"})
			return
		}

		go startCheckPR(&h)
		c.JSON(http.StatusCreated, localMsg{"created"})
	default:
		c.JSON(404, localMsg{"event not supported"})
	}
}

func startCheckPush(hook *models.RepoHook) {
	if strings.Contains(hook.HeadCommit.Message, config.Config.Server.Skip) {
		return
	}

	hasIndexed := false
	r, has, err := models.RepoByOwnerNameInstance(models.RepositoryInstance(hook.Repository), hook.Repository.Owner.UserName, hook.Repository.Name)
	if err != nil {
		finishPush("RepoByOwnerNameInstance", err, hook, "")
		return
	}
	if has {
		has, err = models.HasIndexed(r.ID, hook.HeadCommit.SHA)
		if err != nil {
			finishPush("HasIndexed", err, hook, "")
			return
		}
		hasIndexed = has
	} else {
		// insert new repo
		r = models.LocalRepository(hook.Repository)
		_, err = models.NewRepo(r)
		if err != nil {
			finishPush("NewRepo", err, hook, "")
			return
		}
	}

	if hasIndexed {
		// we do not need to index it again
		return
	}

	var dataDir string
	if config.Config.Data.Tmp {
		dataDir, err = os.MkdirTemp("", "codeberg.org-qwerty287-tea-cloc-*")
		if err != nil {
			finishPush("TempDir", err, hook, "")
			return
		}
	} else {
		dataDir = models.RepositoryDataDir(hook.Repository, config.Config.Data.Location)
	}

	repo, err := git.PlainClone(dataDir, false, &git.CloneOptions{
		Auth:              config.Config.Token(models.RepositoryInstance(hook.Repository)).Git(),
		URL:               hook.Repository.CloneURL,
		Depth:             1,
		RecurseSubmodules: git.NoRecurseSubmodules,
		ReferenceName:     plumbing.ReferenceName(hook.Ref),
	})
	if err != nil {
		finishPush("PlainClone", err, hook, dataDir)
		return
	}

	w, err := repo.Worktree()
	if err != nil {
		finishPush("Worktree", err, hook, dataDir)
		return
	}
	err = w.Checkout(&git.CheckoutOptions{
		Hash:   plumbing.NewHash(hook.HeadCommit.SHA),
		Keep:   false,
		Create: false,
	})
	if err != nil {
		finishPush("Checkout", err, hook, dataDir)
		return
	}

	paths := []string{
		dataDir,
	}

	result, err := processor.Analyze(paths)
	if err != nil {
		finishPush("Analyze", err, hook, dataDir)
		return
	}

	if !config.Config.Data.Keep {
		err := os.RemoveAll(dataDir)
		if err != nil {
			finishPush("RemoveAll", err, hook, "")
			return
		}
	}

	// store to DB
	for _, lang := range result.Languages {
		_, err := models.NewStat(models.FromGocloc(lang, r.ID, hook.HeadCommit.SHA))
		if err != nil {
			log.Printf("NewClient: %v\n", err)
			continue
		}
	}
	if config.Config.Server.AllowPR {
		checkPrUpdates(hook)
	}
	finishPush("", nil, hook, "")
}

func checkPrUpdates(hook *models.RepoHook) {
	// start by loading all PRs
	var pulls []*gitea.PullRequest

	instance := models.RepositoryInstance(hook.Repository)
	c, err := gitea.NewClient(instance, gitea.SetToken(config.Config.Token(instance).Token))
	if err != nil {
		finishPush("NewClient", err, hook, "")
		return
	}

	page := 1
	for {
		prs, _, err := c.ListRepoPullRequests(hook.Repository.Owner.UserName, hook.Repository.Name, gitea.ListPullRequestsOptions{
			State: "open",
			ListOptions: gitea.ListOptions{
				Page: page,
			},
		})
		if err != nil {
			finishPush("ListRepoPullRequests", err, hook, "")
			return
		}
		if len(prs) > 0 {
			pulls = append(pulls, prs...)
			page += 1
		} else {
			break
		}
	}

	for _, v := range pulls {
		if plumbing.NewBranchReferenceName(v.Base.Ref).String() != hook.Ref {
			continue
		}
		// create a dummy PR hook
		prHook := &models.PRHook{
			Number: v.Index,
			PullRequest: &models.PullRequest{
				Number: v.Index,
				Base: &models.Branch{
					Ref:  v.Base.Ref,
					Repo: hook.Repository,
				},
			},
			Repository: hook.Repository,
		}
		runCheckPR(prHook)
	}
}

func finishPush(tag string, err error, hook *models.RepoHook, dataDir string) {
	if dataDir != "" && !config.Config.Data.Keep {
		errRm := os.RemoveAll(dataDir)
		if err != nil {
			err = errRm
		}
	}
	if err != nil {
		SetStatus(hook.Repository, hook.HeadCommit.SHA, gitea.StatusError, fmt.Sprintf("%s: %v\n", tag, err), false)
	} else {
		SetStatus(hook.Repository, hook.HeadCommit.SHA, gitea.StatusSuccess, "", false)
	}
}

func startCheckPR(hook *models.PRHook) {
	time.Sleep(3 * time.Second) // sleep 3 seconds because Gitea takes a short time to update the remote repo
	runCheckPR(hook)
}

func runCheckPR(hook *models.PRHook) {
	instance := models.RepositoryInstance(hook.Repository)
	c, err := gitea.NewClient(instance, gitea.SetToken(config.Config.Token(instance).Token))
	if err != nil {
		finishPr("NewClient", err, hook, "")
		return
	}

	if strings.Contains(hook.PullRequest.Title, config.Config.Server.Skip) {
		return
	} else {
		commit, _, err := c.GetSingleCommit(hook.Repository.Owner.UserName, hook.Repository.Name, hook.PullRequest.Head.SHA)
		if err != nil {
			finishPr("GetSingleCommit", err, hook, "")
		}
		if strings.Contains(commit.RepoCommit.Message, config.Config.Server.Skip) {
			return
		}
	}

	var dataDir string
	if config.Config.Data.Tmp {
		var err error
		dataDir, err = os.MkdirTemp("", "codeberg.org/qwerty287/tea-cloc-*")
		if err != nil {
			finishPr("TempDir", err, hook, "")
			return
		}
	} else {
		dataDir = models.RepositoryDataDir(hook.Repository, config.Config.Data.Location)
	}

	r, err := git.PlainClone(dataDir, false, &git.CloneOptions{
		Auth:              config.Config.Token(models.RepositoryInstance(hook.Repository)).Git(),
		URL:               hook.Repository.CloneURL,
		Depth:             1,
		ReferenceName:     plumbing.NewBranchReferenceName(hook.PullRequest.Base.Ref),
		RecurseSubmodules: git.NoRecurseSubmodules,
	})
	if err != nil {
		finishPr("PlainClone", err, hook, dataDir)
		return
	}

	paths := []string{
		dataDir,
	}

	resultDefault, err := processor.Analyze(paths)
	if err != nil {
		finishPr("Analyze", err, hook, dataDir)
		return
	}

	if err = r.Fetch(&git.FetchOptions{
		Auth:       config.Config.Token(models.RepositoryInstance(hook.Repository)).Git(),
		RemoteName: "origin",
		Tags:       git.NoTags,
		Force:      true,
		RefSpecs: []git_config.RefSpec{
			git_config.RefSpec(fmt.Sprintf("refs/pull/%d/head:refs/remotes/origin/pr-head", hook.Number)),
		},
	}); err != nil {
		finishPr("Fetch", err, hook, dataDir)
		return
	}

	w, err := r.Worktree()
	if err != nil {
		finishPr("Worktree", err, hook, dataDir)
		return
	}

	err = w.Checkout(&git.CheckoutOptions{
		Branch: plumbing.NewRemoteReferenceName("origin", "pr-head"),
		Keep:   false,
		Create: false,
	})
	if err != nil {
		finishPr("Checkout", err, hook, dataDir)
		return
	}

	resultTarget, err := processor.Analyze(paths)
	if err != nil {
		finishPr("Analyze", err, hook, dataDir)
		return
	}

	if !config.Config.Data.Keep {
		err := os.RemoveAll(dataDir)
		if err != nil {
			finishPr("RemoveAll", err, hook, "")
			return
		}
	}

	langs := Calc(resultDefault.Languages, resultTarget.Languages)

	pr, has, err := models.PRByOwnerNameIndex(models.RepositoryInstance(hook.PullRequest.Base.Repo), hook.PullRequest.Base.Repo.Owner.UserName, hook.PullRequest.Base.Repo.Name, hook.Number)
	if err != nil {
		finishPr("PRByOwnerNameIndex", err, hook, "")
		return
	}
	summary, err := createSummaryString(langs)
	if err != nil {
		finishPr("EditIssueComment", err, hook, "")
		return
	}
	if has {
		// there is already a comment
		_, _, err = c.EditIssueComment(hook.PullRequest.Base.Repo.Owner.UserName, hook.PullRequest.Base.Repo.Name, pr.CommentID, gitea.EditIssueCommentOption{
			Body: summary,
		})
		if err != nil {
			finishPr("EditIssueComment", err, hook, "")
			return
		}
	} else {
		// new comment must be created
		comment, _, err := c.CreateIssueComment(hook.PullRequest.Base.Repo.Owner.UserName, hook.PullRequest.Base.Repo.Name, hook.Number, gitea.CreateIssueCommentOption{
			Body: summary,
		})
		if err != nil {
			finishPr("CreateIssueComment", err, hook, "")
			return
		}
		localPR, err := hook.PullRequest.Local()
		if err != nil {
			finishPr("Local", err, hook, "")
			return
		}
		localPR.CommentID = comment.ID
		_, err = models.NewPR(localPR)
		if err != nil {
			finishPr("NewPR", err, hook, "")
		}
	}

	finishPr("", nil, hook, "")
}

type xmlLanguages struct {
	XMLName struct{}    `xml:"languages"`
	Langs   []*Language `xml:"language"`
}

func createSummaryString(langs []*Language) (string, error) {
	switch config.Config.Server.CommentType {
	case "json":
		j, err := json.Marshal(langs)
		if err != nil {
			return "", err
		}
		return string(j), nil
	case "xml":
		x, err := xml.Marshal(&xmlLanguages{Langs: langs})
		if err != nil {
			return "", err
		}
		return string(x), nil
	case "yaml":
		y, err := yaml.Marshal(langs)
		if err != nil {
			return "", err
		}
		return string(y), nil
	}
	res := "|Language|Files|Code|Comments|Blank|Total|\n|---|---|---|---|---|---|"
	for _, lang := range langs {
		res = fmt.Sprintf(
			"%s\n|%s|%d (%s %%)|%d (%s %%)|%d (%s %%)|%d (%s %%)|%d (%s %%)|",
			res,
			lang.Lang().Name,
			len(lang.LanguageTarget.Files),
			addPlus(lang.FilesDiff),
			lang.LanguageTarget.Code,
			addPlus(lang.CodeDiff),
			lang.LanguageTarget.Comments,
			addPlus(lang.CommentsDiff),
			lang.LanguageTarget.Blanks,
			addPlus(lang.BlankDiff),
			lang.LanguageTarget.Total,
			addPlus(lang.TotalDiff),
		)
	}

	if config.Config.Server.CommentType != "table" {
		log.Printf("the comment type %s does not exist. Using table\n", config.Config.Server.CommentType)
	}

	return res, nil
}

func addPlus(val float64) string {
	if val < 0 {
		return strconv.FormatFloat(val, 'f', -1, 64)
	}
	if val == 0 {
		return "±0"
	}
	return fmt.Sprintf("+%s", strconv.FormatFloat(val, 'f', -1, 64))
}

func finishPr(tag string, err error, hook *models.PRHook, dataDir string) {
	errRm := os.RemoveAll(dataDir)
	if err != nil {
		err = errRm
	}
	if err != nil {
		SetStatus(hook.Repository, hook.PullRequest.Head.SHA, gitea.StatusError, fmt.Sprintf("%s: %v\n", tag, err), true)
	} else {
		SetStatus(hook.Repository, hook.PullRequest.Head.SHA, gitea.StatusSuccess, "", true)
	}
}
