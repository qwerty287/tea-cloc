package modules

import (
	"code.gitea.io/sdk/gitea"
	"codeberg.org/qwerty287/tea-cloc/models"
	"codeberg.org/qwerty287/tea-cloc/modules/config"
)

func SetStatus(repo *gitea.Repository, commit string, status gitea.StatusState, desc string, isPR bool) {
	c, err := gitea.NewClient(models.RepositoryInstance(repo), gitea.SetToken(config.Config.Token(models.RepositoryInstance(repo)).Token))
	if err != nil {
		return
	}

	ctx := config.Config.Server.StatusContext
	if isPR {
		ctx = config.Config.Server.StatusContextPR
	}

	targetURL := ""
	if !isPR {
		targetURL = config.FullURL() + repo.Owner.UserName + "/" + repo.Name + "/" + commit + "?instance=" + models.RepositoryInstance(repo)
	}

	_, _, _ = c.CreateStatus(repo.Owner.UserName, repo.Name, commit, gitea.CreateStatusOption{
		State:       status,
		TargetURL:   targetURL,
		Description: desc,
		Context:     ctx,
	})
}
