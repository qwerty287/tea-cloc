package modules

import (
	"github.com/hhatto/gocloc"
)

type Language struct {
	CodeDiff       float64
	FilesDiff      float64
	CommentsDiff   float64
	BlankDiff      float64
	TotalDiff      float64
	LanguageBase   *gocloc.Language
	LanguageTarget *gocloc.Language
}

func (l *Language) Lang() *gocloc.Language {
	if l.LanguageTarget.Name != "" {
		return l.LanguageTarget
	}
	return l.LanguageBase
}

func (l *Language) calcPercentages() {
	if l.LanguageBase == nil {
		// language was added completely
		if l.LanguageTarget.Code != 0 {
			l.CodeDiff = 100
		} else {
			l.CodeDiff = 0
		}
		if len(l.LanguageTarget.Files) != 0 {
			l.FilesDiff = 100
		} else {
			l.FilesDiff = 0
		}
		if l.LanguageTarget.Comments != 0 {
			l.CommentsDiff = 100
		} else {
			l.CommentsDiff = 0
		}
		if l.LanguageTarget.Blanks != 0 {
			l.BlankDiff = 100
		} else {
			l.BlankDiff = 0
		}
		if l.LanguageTarget.Total != 0 {
			l.TotalDiff = 100
		} else {
			l.TotalDiff = 0
		}
		return
	}
	if l.LanguageTarget == nil {
		// language was removed completely
		l.LanguageTarget = &gocloc.Language{
			Files:    []string{},
			Code:     0,
			Comments: 0,
			Blanks:   0,
			Total:    0,
		}
		l.CodeDiff = -100
		l.FilesDiff = -100
		l.CommentsDiff = -100
		l.BlankDiff = -100
		l.TotalDiff = -100
		return
	}
	// now we have to calculate
	if l.LanguageBase.Code != 0 {
		l.CodeDiff = float64(l.LanguageTarget.Code)/float64(l.LanguageBase.Code)*100 - 100
	} else {
		if l.LanguageTarget.Code == 0 {
			l.CodeDiff = 0
		} else {
			l.CodeDiff = 100
		}
	}
	if len(l.LanguageBase.Files) != 0 {
		l.FilesDiff = float64(len(l.LanguageTarget.Files))/float64(len(l.LanguageBase.Files))*100 - 100
	} else {
		if len(l.LanguageTarget.Files) == 0 {
			l.FilesDiff = 0
		} else {
			l.FilesDiff = 100
		}
	}
	if l.LanguageBase.Comments != 0 {
		l.CommentsDiff = float64(l.LanguageTarget.Comments)/float64(l.LanguageBase.Comments)*100 - 100
	} else {
		if l.LanguageTarget.Comments == 0 {
			l.CommentsDiff = 0
		} else {
			l.CommentsDiff = 100
		}
	}
	if l.LanguageBase.Blanks != 0 {
		l.BlankDiff = float64(l.LanguageTarget.Blanks)/float64(l.LanguageBase.Blanks)*100 - 100
	} else {
		if l.LanguageTarget.Blanks == 0 {
			l.BlankDiff = 0
		} else {
			l.BlankDiff = 100
		}
	}
	if l.LanguageBase.Total != 0 {
		l.TotalDiff = float64(l.LanguageTarget.Total)/float64(l.LanguageBase.Total)*100 - 100
	} else {
		if l.LanguageTarget.Total == 0 {
			l.TotalDiff = 0
		} else {
			l.TotalDiff = 100
		}
	}
}

func Calc(base, target map[string]*gocloc.Language) []*Language {
	var langs []*Language
	for k, v := range base {
		langs = append(langs, &Language{
			LanguageBase:   v,
			LanguageTarget: target[k],
		})
	}
	for k, v := range target {
		_, has := base[k]
		if !has {
			langs = append(langs, &Language{
				LanguageBase:   nil,
				LanguageTarget: v,
			})
		}
	}
	for _, l := range langs {
		l.calcPercentages()
	}
	return langs
}
