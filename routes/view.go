package routes

import (
	"fmt"
	"log"
	"net/http"
	"strings"

	"codeberg.org/qwerty287/tea-cloc/models"

	"github.com/gin-gonic/gin"
)

type xmlLanguages struct {
	XMLName struct{}               `xml:"language_stats"`
	Langs   []*models.LanguageStat `xml:"language_stat"`
}

func ViewCount(c *gin.Context) {
	instance := c.DefaultQuery("instance", "")
	var r *models.LocalRepo
	if instance == "" {
		repos, err := models.ReposByOwnerName(c.Param("owner"), c.Param("repo"))
		if err != nil {
			log.Println(err)
			c.String(http.StatusInternalServerError, "error while reading DB")
			return
		}
		if len(repos) == 0 {
			c.String(http.StatusNotFound, "not found")
			return
		} else if len(repos) > 1 {
			c.String(http.StatusNotFound, "found multiple repos with different instances, please specify instance")
			return
		} else {
			r = repos[0]
		}
	} else {
		var has bool
		var err error
		r, has, err = models.RepoByOwnerNameInstance(instance, c.Param("owner"), c.Param("repo"))
		if err != nil {
			log.Println(err)
			c.String(http.StatusInternalServerError, "error while reading DB")
			return
		}
		if !has {
			c.String(http.StatusNotFound, "not found")
			return
		}
	}
	langs, err := models.StatsByRepoSHA(r.ID, c.Param("sha"))
	if err != nil {
		log.Println(err)
		c.String(http.StatusInternalServerError, "error while reading DB")
		return
	}
	if len(langs) < 1 {
		c.String(http.StatusNotFound, "not found")
		return
	}
	switch strings.ToLower(c.DefaultQuery("format", "table")) {
	case "json":
		c.JSON(http.StatusOK, langs)
		return
	case "table":
		c.Header("Content-Type", "text/html")
		c.String(http.StatusOK, htmlTable(langs))
		return
	case "xml":
		xmlStruct := &xmlLanguages{
			Langs: langs,
		}
		c.XML(http.StatusOK, xmlStruct)
		return
	case "yaml":
		c.YAML(http.StatusOK, langs)
		return
	default:
		c.Header("Content-Type", "text/html")
		c.String(http.StatusNotFound, "unknown format")
		return
	}
}

func htmlTable(stats []*models.LanguageStat) string {
	res := "<table><th>Language</th><th>Files</th><th>Code</th><th>Comments</th><th>Blanks</th><th>Total</th><tr>"
	for _, v := range stats {
		res = fmt.Sprintf(
			"%s<tr><td>%s</td><td>%d</td><td>%d</td><td>%d</td><td>%d</td><td>%d</td></tr>",
			res,
			v.Name,
			v.FilesNum,
			v.Code,
			v.Comments,
			v.Blanks,
			v.Total,
		)
	}
	return fmt.Sprintf("%s</table>", res)
}
